import os
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
import time
from datetime import datetime

"""
Displays a welcome message on main page and then at /store/<name> endpoint stores a name to 
database
"""
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_NAME = os.environ.get("DB_NAME")
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = f"postgres://postgres:guest@{DB_HOST}:{DB_PORT}/{DB_NAME}"
db = SQLAlchemy(app)
time.sleep(5)


# Define table for user registrations
class Registration(db.Model):

    # Auto generating an id like this for an application within the database is generally not a good idea
    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    name = db.Column("name", db.String(200), nullable=True)
    timestamp = db.Column("timestamp", db.Integer)


@app.route("/newuser/<username>")
def new_user(username):

    new_user = Registration(name=username, timestamp=int(datetime.utcnow().timestamp()))
    db.session.add(new_user)
    db.session.commit()

    return f"Added {username} to DB"


if __name__ == '__main__':

    db.create_all()
    app.run(host="0.0.0.0", port="5000")
