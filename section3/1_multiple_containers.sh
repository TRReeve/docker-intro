# In this example we've taken an application with two containers the application and the database
# and composed them together inside a docker compose file.
# This allows us to define how several containers should work together in one self contained "network"
#
#TO START
docker-compose -f docker-compose.yml up -d --build

# Go to http://localhost:9001/newuser/mynewuser

# Then exec into your database container and using psql -d webappdb -U postgres and run a query all in one line using
docker exec section3_backend_db_1 psql -d webappdb -U postgres -c "Select * from registration"

# You should be able to see the users you've created in the result

# TO FINISH
docker-compose -f docker-compose.yml down --remove-orphans
