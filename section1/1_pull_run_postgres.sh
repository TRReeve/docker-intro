# Get the postgres image from Dockerhub (defaults to latest image)
# https://hub.docker.com/
docker pull postgres
#OR
#docker pull postgres:latest

# Specify a specific version using a : tag
#docker pull postgres:11

# Run the container. Normally well maintained containers in dockerhub will
# have documentation and instructions e.g https://hub.docker.com/_/postgres?tab=description
# --rm removes the image after
docker run -p 5432:5432 --name postgres_test -e POSTGRES_PASSWORD=mypass postgres

#You'll see logs for a postgres database appear.

# OPEN A NEW TERMINAL

# List out all the running docker containers
docker ps
#CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
#1d98cdf69c4e        postgres            "docker-entrypoint.s…"   4 minutes ago       Up 4 minutes        0.0.0.0:5432->5432/tcp   postgres_test

#Move into the container
docker exec -it postgres_test bash

# Open the postgres database terminal, have a play around you're now in postgres world.
# psql -U postgres -d postgres

# Go back to your terminal with the running postgres instance and press ctrl + c to finish the session
# Verify the session is closed by running
docker ps
