# Repeat the previous exercise, but now we'll run in detached mode so you can run several containers
docker run -p 5432:5432 -d --rm --name db1 -e POSTGRES_PASSWORD=mypass postgres

# Now we want two seperate databases with different versions on the same computer
# NOTICE how we're mapping 5432 on container port to 5433 on the host machine (your computer)
docker run -p 5433:5432 -d --rm --name db2 -e POSTGRES_PASSWORD=mypass postgres:9

# Run a command in first container but remain on your "host" machine to get version
docker exec db1 psql --version

# Run a command in second container but remain on your "host" machine to get version
docker exec db2 psql --version

# Go into your second container
docker exec -it db2 bash

# Run the same command from inside the container and get the same output
psql --version
# psql (PostgreSQL) 9.6.19

# You're now running two different versions of postgres on the same computer.
# If you have a SQL database, you can connect to different versions on port 5432 and 5433 on your computer
# at localhost

# Shut down the background running containers
docker stop db1
docker stop db2
# OR docker stop db1 db2
