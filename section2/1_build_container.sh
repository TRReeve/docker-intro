# In this exercise we now have some code in our application directory
# and we have a special file called "Dockerfile"

# --BUILD THE CONTAINER --
docker build -t webapp .
# This will pull and download the image for you in layers e.g
# ---> Running in a490458411f8
#Removing intermediate container a490458411f8
# ---> b5a6053ce8be
#Step 5/5 : CMD ["python3", "app.py"]
# ---> Running in 6447b21a6ce5
#Removing intermediate container 6447b21a6ce5
# ---> 59819f056d36

# Now you have an image stored locally called webapp
# run the image, but change the start script to the commaned "bash"
docker run --entrypoint bash -it webapp

# Now go inside the container (hint: section1 and the exec command)
# THen run the command ls inside the container
# see how your code has been copied inside to app.py
# app.py  bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var


# Now run the container mapping port 8080 in the container to port 9000 on the host machine
docker run -p 9000:8080 -d --name addapp1 webapp

# Go to http://localhost:9000/json_add/9/5 in your browser (or use curl in another terminal) to see the app running






