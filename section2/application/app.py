from flask import Flask, jsonify

"""
Displays a welcome message on main page and then adds two numbers and returns the result 
in a JSON object. 
"""
app = Flask(__name__)


@app.route("/")
def message_one():

    return "Message from webapp"


@app.route("/json_add/<x>/<y>")
def message_two(x, y):

    try:
        x_int = int(x)
        y_int = int(y)
        ans_int = x_int + y_int
        response_dict = dict(x=x_int, y=y_int, answer=ans_int)

    except Exception as e:
        response_dict = dict(message="Error")

    return jsonify(response_dict)


if __name__ == '__main__':

    app.run(port=8080, host="0.0.0.0")